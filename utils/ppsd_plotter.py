#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)

 Usage:
    ppsd_plotter arclink <start> <end> <stream> [-S <server>] [-P <port>] \
[-U <user>] [--sensitivity=<sens>] [--period=<per>] [--damping=<damp>] \
[--unit=<unit>]
    ppsd_plotter fdsn <start> <end> <stream> [--url <url>] \
[--sensitivity=<sens>] [--period=<per>] [--damping=<damp>] [--unit=<unit>]
    ppsd_plotter mseed <file> [--sensitivity=<sens>] [--period=<per>] \
[--damping=<damp>] [--unit=<unit>]

Example:
    ppsd_plotter arclink 2016-09-30T01:00:00 2016-09-30T05:00:00 \
FR.STR.00.HHZ,FR.CHMF.00.HHZ -S renass-fw -P 18001

Options:
    -h --help           Show this screen.
    --version           Show version.
    <start>             Start time in obspy.core.UTCDateTime format. \
Arclink or fdsn mode only.
    <end>               End time in obspy.core.UTCDateTime format. \
Arclink or fdsn mode only.
    <stream>            Stream to analyse in seed code separated by dots.\
Arclink or fdsn mode only.
    -S <server>         Set address of arclink server, arclink mode only. \
[default: renass-fw].
    -P <port>           Set port of arclink server, arclink mode only. \
[default: 18001].
    -U <user>           Set user for connecting to arclink server \
[default: pfi@unistra.fr].
    --url <url>         Set base url of fdsnws server, fdsn mode only. \
[default: http://ws.resif.fr]
    --sensitivity=<sens>   Specify overall sensitivity [default: 4.804e8].
    --period=<per>      Specify expected period [default: 120].
    --damping=<damp>    Specify expected damping [default: 0.707].
    --unit=<unit>       Specify output unit [default: M/S].
"""

from docopt import docopt
from obspy.clients.arclink import Client as aClient
from obspy.clients.fdsn import Client as fClient
from obspy.core import UTCDateTime, read
from obspy.signal.invsim import corn_freq_2_paz
from obspy.signal.spectral_estimation import PPSD


def make_paz(_period, _damping, _sensitivity, _unit):
    paz = corn_freq_2_paz(1./_period, damp=_damping)
    paz['sensitivity'] = _sensitivity
    paz['sensitivity_unit'] = _unit
    return paz


if __name__ == '__main__':
    args = docopt(__doc__, version='ppsd_plotter 1.0')
    # Uncomment for debug
    # print(args)

    if args['arclink']:
        data = aClient(host=args['-S'], port=args['-P'],
                       user=args['-U'])
        t1 = UTCDateTime(args['<start>'])
        t2 = UTCDateTime(args['<end>'])
        code = args['<stream>'].split('.')
        st = data.get_waveforms(code[0], code[1], code[2], code[3], t1,
                                t2, route=False)
        st.merge()

    elif args['fdsn']:
        data = fClient(base_url=args['--url'])
        t1 = UTCDateTime(args['<start>'])
        t2 = UTCDateTime(args['<end>'])
        code = args['<stream>'].split('.')
        st = data.get_waveforms(code[0], code[1], code[2], code[3], t1,
                                t2)

    elif args['mseed']:
        st = read(args['<file>'])

    print(st)
    st.plot(equal_scale=False)

    try:
        if args['arclink']:
            paz_mes = data.get_paz(st[0].stats.network, st[0].stats.station,
                                   st[0].stats.location, st[0].stats.channel,
                                   t1, route=False)
            paz_mes = dict(paz_mes)
        elif args['fdsn']:
            paz_mes = data.get_stations(network=st[0].stats.network,
                                        station=st[0].stats.station,
                                        location=st[0].stats.location,
                                        channel=st[0].stats.channel,
                                        starttime=t1, endtime=t2,
                                        level='response')
        elif args['mseed']:
            paz_mes = make_paz(float(args['--period']),
                               float(args['--damping']),
                               float(args['--sensitivity']), args['--unit'])

    except:
        paz_mes = make_paz(float(args['--period']), float(args['--damping']),
                           float(args['--sensitivity']), args['--unit'])

    ppsd = PPSD(st[0].stats, paz_mes)
    ppsd.add(st)
    ppsd.plot(show=False)
    ppsd.plot_spectrogram(show=False)
    ppsd.plot_temporal([0.1, 5, 100])
