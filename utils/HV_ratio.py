#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Created on Thu Apr 25 15:26 2019
    
    @author: Celeste
    
    Create a Horizontal/Vertical ratio from data contained in fdsn/arclink server or mseed file.
    
    Usage:
    HV_ratio.py -m <demean_factor> -t <detrend_factor> -w <water_level>
    
    Example:
    HV_ratio -m
    
    Options:
    -h --help           Show this screen.
    --version           Show version.
    - r                 Output repertory containing the daily data
    - l                 List of the concerned stations
    """

from docopt import docopt
from obspy.clients.arclink import Client as aClient
from obspy.clients.fdsn import Client as fClient
from obspy.core import UTCDateTime, read
from obspy.signal.invsim import corn_freq_2_paz
from obspy.signal.spectral_estimation import PPSD

if __name__ == '__main__':
    args = docopt(__doc__, version='HV_ratio 1.0')
    # Uncomment for debug
    # print(args)
    
    if args['arclink']:
        data = aClient(host=args['-S'], port=args['-P'],
                       user=args['-U'])
                       t1 = UTCDateTime(args['<start>'])
                       t2 = UTCDateTime(args['<end>'])
                       code = args['<stream>'].split('.')
                       st = data.get_waveforms(code[0], code[1], code[2], code[3], t1,
                                               t2, route=False)
                       st.merge()

    elif args['fdsn']:
        data = fClient(base_url=args['--url'])
        t1 = UTCDateTime(args['<start>'])
        t2 = UTCDateTime(args['<end>'])
        code = args['<stream>'].split('.')
        st = data.get_waveforms(code[0], code[1], code[2], code[3], t1,
                            t2)
        
    elif args['mseed']:
        st = read(args['<file>'])
