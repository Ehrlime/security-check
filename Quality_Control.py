#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 14 18:06:41 2018

@author: Celeste

Quality Control of the CLB network. The PPSD plotter function has originally 
been created by Maxime Bès de Berc (mbesdeberc@unistra.fr)

Usage:
    python3 security-check.py confSC.yaml

Options:
    -h --help           Show this screen.
    --version           Show version.
"""



import argparse
import glob
from lxml import etree
from matplotlib import pyplot
from obspy.clients.fdsn import Client
from utils import _HV_ratio, _ppsd_plotter, _scan
import yaml
import zipfile


class QualityControl:
		
    def __init__(self, conf):
        self.HV_ratio = list()
        self.ppsd_plotter = list()
        self.scan = list()
        self.server = Client(base_url='http://renass.unistra.fr:8080')
        self.stations_list

#Déterminer les requêtes depuis les infos du yaml ?

print("Global statistics for the last 24h:")

# Définir les fonctions

    def HV_ratio(self, conf):
        _HV_ratio(self.HV_ratio, self.server, conf)

    def ppsd_plotter(self, conf):
        _ppsd_plotter(self.ppsd_plotter, self.server, conf)

    def scan(self, conf):
        _scan(self.scan, self.server, conf)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='run security_check')
    parser.add_argument('conf_file', help='path configuration file in yaml \
                        format')
    args = parser.parse_args()

    security_check = QualityControl()

    conf_file = open(args.conf_file)
    conf = yaml.load(conf_file)
    conf_file.close()

    security_check = utils(conf)

    for station in stations_list(conf)
        security_check.xml_path #A chaque station son .xml
        security_check.sds_path #Chercher les fichiers archivés pour ensuite
                                # effectuer les codes
        security_check.HV_ratio(conf)
        security_check.scan(conf)
        security_check.ppsd_plotter(conf)
        security_check.plot()