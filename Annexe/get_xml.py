#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 14 18:06:41 2018

@author: Celeste

Create from a list of stations a .xml file, gathering all the dataless

Usage:
    get_xml.py list -l <stations_list> -r <output_repertory>

Example:
    get_xml -l stations_list.txt -r /home/broucke/Security_check/Daily_check

Options:
    -h --help           Show this screen.
    --version           Show version.
    - r                 Output repertory containing the daily data
    - l                 List of the concerned stations  
"""

from obspy.clients.fdsn import Client
from obspy import UTCDateTime
from obspy import read_inventory
import yaml  


conffile = open("conf.yaml")
conf = yaml.load(conffile)
conffile.close()

for stat in conf["stations"] :
    client = Client(base_url="RESIF")
    start = UTCDateTime.now() - 86400
    end = UTCDateTime.now()
    inventory = client.get_stations(network="FR", station=stat, 
                    level="response",starttime=start, endtime=end, 
                    filename=conf['xml_path'] + stat + ".xml")
    print(inventory)

    

        
        
        